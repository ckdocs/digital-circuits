# Combinational Circuits

## Multiplexer

> [!NOTE]
> Or `selector`

### 2x1

![Multiplexer 2x1](assets/multiplexer_2x1.png)

### 4x1

![Multiplexer 4x1](assets/multiplexer_4x1.png)

### 4x1 using 2x1

![Multiplexer 4x1 using 2x1](assets/multiplexer_4x1_2x1.png)

### 16x1 using 4x1

![Multiplexer 16x1 using 4x1](assets/multiplexer_16x1_4x1.png)

---

### Synthesis

> [!TIP]
> Synthesis circuits by merging every `two row`
>
> Or you can remerge the `two rows` that was merged before

![Multiplexer Synthesis 1](assets/multiplexer_synthesis_1.png)

![Multiplexer Synthesis 2](assets/multiplexer_synthesis_2.png)

![Multiplexer Synthesis 3](assets/multiplexer_synthesis_3.png)

---

#### Shannon's Expansion

> [!TIP]
>
> **Shannon's Expansion Theorem**: Any boolean function $f(w_1,w_2,\dots,w_n)$ can be written in the form:
>
> $$
> \begin{aligned}
> & f(w_1,w_2,\dots,w_n) = \overline{w_1}.f(0,w_2,\dots,w_n) + w_1.f(1,w_2,\dots,w_n)
> \end{aligned}
> $$

Example:

![Multiplexer Synthesis Shannon](assets/multiplexer_synthesis_shannon.png)

---

## Decoder

> [!NOTE]
>
> Decoder `decompresses` the input

### 2x4

![Decoder 2x4](assets/decoder_2x4.png)

### 2x4 + Enable

![Decoder 2x4 Enable](assets/decoder_2x4_enable.png)

### 3x8 using 2x4

![Decoder 3x8 using 2x4](assets/decoder_3x8_2x4.png)

### 4x16 using 2x4

![Decoder 4x16 using 2x4](assets/decoder_4x16_2x4.png)

---

## Demultiplexer

> [!TIP]
>
> Demultiplexer is a **Reverse Decoder** (`f` is `Enable` wire)

![Demultiplexer](assets/demultiplexer.png)

---

## Encoder

> [!NOTE]
>
> Encoder `compresses` the input

### 4x2

![Encoder 4x2](assets/encoder_4x2.png)

### Priority Encoder

![Encoder Priority](assets/encoder_priority.png)

---

## Arithmetic Comparision

1. Equal
2. Grater
3. Less

![Arithmetic Comparision](assets/arithmetic_comparision.png)

Detail:

![Arithmetic Comparision Detail](assets/arithmetic_comparision_detail.png)

---
