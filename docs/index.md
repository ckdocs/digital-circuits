# Digital Circuits

Logical Circuits tutorial

---

# Index

1. Digital vs Analog
2. Digital Logic
    1. AND, OR, NOT basic gates
    2. NAND, NOR, EOR, ENOR advanced gates
    3. Logic Functions
    4. Logic Networks (Gates Schema)
    5. High/Low Voltage
    6. Combinational/Sequential circuits
    7. Truth Table
        1. Proof
    8. Timing Diagram (Analysis Synthesis)
        1. Proof
    9. Venn Diagram
        1. Proof
    10. Boolean Algebra
        1. DeMorgan's theorem
    11. Minterm, Maxterm
        1. SOP (Sum Of Products)
            1. NAND for minterms
        2. POS (Product Of Sums)
            1. NOR for maxterms
        3. SOP <=> POS
        4. Synthesis (Truth Table => SOP/POS)
        5. Minimization and Karnaugh Maps
            1. Don't care bits (`X`)
    12. Cost Criteria
        1. Literal
        2. Term
        3. Not
        4. L, G, GN(Cost)
3. Numbers representations
    1. Unsigned
    2. Signed
        1. Sign and Magnitude
        2. 1's Complement
        3. 2's Complement
    3. Adder
        1. Ripple Carry Adder
        2. Carry Lookahead Adder
    4. Multiplier
4. Digital CAD Tools
    1. Schematic Capture
        1. Proteus
        2. ModelSIM
        3. OrCAD
    2. HDL (Hardware Description Language)
        1. VHDL
        2. Verilog HDL

---
