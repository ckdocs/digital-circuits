# Digital vs Analog

![Digital vs Analog](assets/digital_vs_analog.png)

| Digital                  | Analog                  |
| ------------------------ | ----------------------- |
| Low cost                 | More cost               |
| Easy adjustment          | Tight adjustment        |
| More resistance to noise | Low resistance to noise |

---
