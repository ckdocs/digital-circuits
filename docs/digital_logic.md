# Digital Logic

![Digital Logic](assets/digital_logic_1.png)
![Digital Logic](assets/digital_logic_2.png)
![Digital Logic](assets/digital_logic_3.png)
![Digital Logic](assets/digital_logic_4.png)
![Digital Logic](assets/digital_logic_5.png)
![Digital Logic](assets/digital_logic_6.png)

## AND, OR, NOT basic gates

![Basic_Gates](assets/and_or_not.jpg)
![Basic_Gates](assets/and_or_not.png)

$$
\begin{aligned}
A \: and \: B &\equiv A.B
\\
A \: or \: B &\equiv A+B
\\
not \: A &\equiv !A \equiv \overline{A}
\end{aligned}
$$

---

## NAND, NOR, EOR, ENOR advanced gates

![Advanced_Gates](assets/logic_gates.png)

$$
\begin{aligned}
EOR &\equiv XOR
\\
ENOR &\equiv XNOR
\end{aligned}
$$

### Tip

![Advanced Gates](assets/nand_nor_eor_enor_tip.jpg)

---

## Logic Functions

A logical mathematical representation.

### Example

$$
\begin{aligned}
E = (A.B.\overline{C}) + (A.\overline{B}.C) + (\overline{A}.B.C)
\end{aligned}
$$

---

## Logic Networks (Gates Schema)

Combination of logical gates.

> A graphical representation of a logic functions

![Logic Network](assets/logic_network_1.png)
![Logic Network](assets/logic_network_2.png)

---

## High/Low Voltage

### High Voltage

Means `assert` or `1` or `true`.

### Low Voltage

Means `dessert` or `0` or `false`.

---

## Combinational/Sequential Circuits

![Combinational vs Sequential](assets/combinational_vs_sequential.jpg)

### Combinational Circuits

Circuits without any memory.

> Output only depends on inputs

### Sequential Circuits

Circuits with memory.

> Output depends on inputs and memories states

---

## Truth Table

![Truth Table](assets/truth_table_1.png)

### Tip

$$
\begin{aligned}
P: T,T,T,T,F,F,F,F &\equiv 4,4
\\
Q: T,T,F,F,T,T,F,F &\equiv 2,2,2,2
\\
R: T,F,T,F,T,F,T,F &\equiv 1,1,1,1,1,1,1
\end{aligned}
$$

### Tip

$$
\begin{aligned}
&P,Q,R: 000
\\
&Every time: +001
\\
\\
&Example:
\\
&PQR:
\\
\\
&000
\\
&001
\\
&010
\\
&011
\\
&100
\\
&101
\\
&110
\\
&111
\end{aligned}
$$

### Proof

We have to show, equality of LHS, RHS (`Truth table of Left Logic Function` and `Truth table of Right Logic Function`)

![Timing Diagram Proof](assets/timing_diagram_proof.png)

$\overline{x.y} \: \equiv \: \overline{x} + \overline{y}$

#### Tip

`Draw Truth Table` for every part of logic function, for example:

F = (a.b)+(c.d)

1. a
2. b
3. c
4. d
5. (a.b)
6. (c.d)
7. (a.b)+(c.d)

---

## Timing Diagram (Analysis Synthesis)

> A graphical representation of a truth table, that `time` is `vertical` line over truth table

![Timing Diagram](assets/timing_diagram_1.png)

### Example

$F = a \: xor \: b \: xor \: c$

![Timing Diagram](assets/timing_diagram_2.jpg)

### Proof

We have to show, equality of LHS, RHS (`Timing Diagram of Left Logic Function` and `Timing Diagram of Right Logic Function`)

#### Tip

`Draw Timing Diagram` for every part of logic function, for example:

F = (a.b)+(c.d)

1. a
2. b
3. c
4. d
5. (a.b)
6. (c.d)
7. (a.b)+(c.d)

---

## Venn Diagram

![Venn Diagram](assets/venn_diagram_1.jpg)

### Example (proof)

![Venn Diagram](assets/venn_diagram_2.png)

### Proof

We have to show, equality of LHS, RHS (`Venn Diagram of Left Logic Function` and `Venn Diagram of Right Logic Function`)

#### Tip

`Draw Venn Diagram` for every part of logic function, for example:

F = (a.b)+(c.d)

1. a
2. b
3. c
4. d
5. (a.b)
6. (c.d)
7. (a.b)+(c.d)

---

## Boolean Algebra

![Boolean Rules](assets/boolean_rules.jpg)

### DeMorgan's theorem

![DeMorgan](assets/boolean_algebra_demorgan.png)

---

## Minterm, Maxterm

We can describe all of the logic functions truth table using minterm or maxterms, then using karnough tables we can simplify them.

![Minterm Maxterm](assets/minterm_maxterm.png)

### Example

We can convert every truth table to SOP or POS:

**Example1**:
![Minterm Maxterm](assets/minterm_maxterm_example.png)

**Example2**:
![Minterm Maxterm](assets/minterm_maxterm_example_2.png)

**Example3**:
![Minterm Maxterm](assets/minterm_maxterm_example_3.png)

### SOP (Sum Of Products)

Or of 1's

> F = 1 or 1 or 1 or 1

![Minterm Maxterm](assets/minterm_maxterm_example_4.png)

#### NAND for minterms

> We can implement SOP or sum of minterms using `only NAND` gates

![NAND](assets/minterm_nand.png)

---

### POS (Product Of Sums)

And of 0's

> F = 0 and 0 and 0 and 0

![Minterm Maxterm](assets/minterm_maxterm_example_5.png)

![Minterm Maxterm](assets/minterm_maxterm_example_6.png)

#### NOR for maxterms

> We can implement POS or product of maxterms using `only NOR` gates

![NOR](assets/maxterm_nor.png)

---

### SOP <=> POS

Using these `three rules`:

> Sigma(m(0,1,2,3)) = Pi(M(4,5,6,7)) </br>
> Sigma(m(0,1,2,3)) = Not(Sigma(m(4,5,6,7))) </br>
> Pi(M(0,1,2,3)) = Not(Pi(m(4,5,6,7)))

$$
\begin{aligned}
&f = \Sigma m(1,2,5,6) = \overline{\Pi M(1,2,5,6)} = \Pi M(0,3,4,7)
\\
&\overline{f} = \overline{\Sigma m(1,2,5,6)} = \Pi M(1,2,5,6) = \Sigma m(0,3,4,7)
\end{aligned}
$$

![Minterm Maxterm](assets/minterm_maxterm_example_7.png)

---

### Synthesis (Truth Table => SOP/POS)

![Synthesis](assets/synthesis_example.jpg)

---

### Minimization and Karnaugh Maps

Minimization can be done using:

    1. Boolean algebra
       1. A.(B+C) = A.B + A.C
       2. A+(B.C) = (A+B).(A+C)
    2. Karnaugh maps

1. Find the K-Map size
    1. 2 vars: 2x2 => X1 / X2
    2. 3 vars: 2x4 => X1,X2 / X3
    3. 4 vars: 4x4 => X1,X2 / X3,X4
    4. 5 vars: 4x8 => X1,X2,X3 / X4,X5
2. Insert bits
    1. Every two adjacent blocks should have only `one bit` with `different values`
3. Insert X1,X2,...,Xn
    1. Per rows, columns => Every blocks with value 1 of that bit are in that bit group
4. Insert `minterms (1)`/ `maxterms (0)` based on `bit numbers`
5. Grouping with size of power of 2, try find `max size` to `low size`
6. Write groups using X1,X2,...,Xn

#### Don't care bits (`X`)

We insert don't care bits as `X` (not `1` or `0` for minterm or Maxterms) and use it for simplifying in K-Map table both in `MinTerm` and `MaxTerm` :)

#### Example

![Example](assets/three_groupings.jpg)

![Minimization K-Map](assets/minimization_example_1.png)

---

## Cost Criteria

We can find the cost of a logic function

### Literal

Every variable in `F` function called a literal:

```code
F = A.B.C + !A.!B.C + !A.B.C
    1+1+1 +  1+1+1  +  1+1+1 = 9
```

**Literal Cost**: 9

---

### Term

Every `POS` or `SOP` called a term:

```code
F = A.B.C + !A.!B.C + !A.B.C
      1   +     1   +    1 = 3
```

**Term Cost**: 3

---

### Not

Every `NOT` gate overy literals:

```code
F = A.B.C + !A.!B.C + !A.B.C
    0+0+0 +  1+1+0  +  1+0+0 = 3
```

**Not Cost**: 3

---

### L, G, GN(Cost)

Literal Cost(`L`) = `Literals`

Gate Cost(`G`) = `Literals` + `Terms`

Gate with Not(`GN`) = `Literals` + `Terms` + `Nots` == Cost

![Cost](assets/cost.jpg)

---
