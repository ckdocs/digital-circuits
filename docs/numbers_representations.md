# Numbers representations

## Integer

### Unsigned

![Unsigned Number](assets/unsigned_number.png)

Binary:

$$
\begin{aligned}
& B = b_{n-1} \: b_{n-2} \: \dots \: b_{1} \: b_{0}
\\
\\
& Values(B) = \sum_{i=0}^{n-1} b_{i} * 2^{i}
\end{aligned}
$$

Octal, Hexadecimal:

$$
\begin{aligned}
& K = k_{n-1} \: k_{n-2} \: \dots \: k_{1} \: k_{0}
\\
\\
& Values(K) = \sum_{i=0}^{n-1} k_{i} * r^{i}
\end{aligned}
$$

#### Half Adder

```code
Input:
X: digit
Y: digit

Output:
S: sum = X xor Y
C: carry = X and Y
```

![Half Adder](assets/half_adder_1.png)

![Half Adder](assets/half_adder_2.png)

![Half Adder](assets/half_adder_3.png)

---

#### Full Adder

```code
Input:
X: digit
Y: digit
C: previews carry

Output:
S: sum = X xor Y xor C
C: next carry = (X and Y) or (X and C) or (Y and C)
```

![Full Adder](assets/full_adder_1.png)

![Full Adder](assets/full_adder_2.png)

![Full Adder](assets/full_adder_3.png)

##### Implement using `Half Adder`

![Full Half Adder](assets/full_half_adder_1.png)

![Full Half Adder](assets/full_half_adder_2.png)

---

#### Ripple-Carry Adder

![Ripple Carry Adder](assets/ripple_carry_adder.png)

##### Arithmetic Overflow

When the summation overflows the size of bits

![Arithmetic Overflow](assets/full_adder_overflow.png)

```js
let A_Sign = A[n - 1];
let B_Sign = B[n - 1];

if (A_Sign == 0 && B_Sign == 0) {
    // A,B are +
    if (Sum[3] == 1) {
        /*
            101
        +   100
        --------
           1001

           Sum[3] = (1) 0 0 1 => 1
        */
        Overflow;
    }
} else if (A_Sign == 1 && B_Sign == 1) {
    // A,B are -
    if (Sum[3] == 0) {
        /*
            001
        +   100
        --------
           0101

           Sum[3] = (0) 1 0 1 => 0
        */
        Overflow;
    }
}
```

$$
\begin{aligned}
& Overflow = \overline{c_3}.c_4 + c_3.\overline{c_4} = c_3 \oplus c_4
\\
& Overflow = x_3.y_3.\overline{s_3} + \overline{x_3}.\overline{y_3}.s_3
\end{aligned}
$$

---

##### Performance Issue

> [!WARNING]
>
> **Long** pass through

![Ripple Carry Adder Detail](assets/ripple_carry_adder_detail.png)

$$
\begin{aligned}
& c_{i+1} = x_i.y_i + x_i.c_i + y_i.c_i
\\
& c_{i+1} = x_i.y_i + (x_i + y_i).c_i
\\
\\
& g_i = x_i.y_i
\\
& p_i = x_i + y_i
\\
\\
& c_{i+1} = g_i + p_i.c_i
\end{aligned}
$$

Example:

$$
\begin{aligned}
& c_1 = g_0 + p_0.c_0
\\
& c_2 = g_1 + p_1.c_1
\\
& c_3 = g_2 + p_2.c_2
\\
& \dots
\end{aligned}
$$

---

#### Carry-Lookahead Adder

Fixes the `Ripple Carry Adder` performance issue using remove the recursive gates `=>` replace them by new copied gates.

![Carry Lookahead Adder](assets/carry_lookahead_adder.png)

Example:

$$
\begin{aligned}
& c_1 = g_0 + p_0.c_0
\\
& c_2 = g_1 + p_1.(g_0 + p_0.c_0)
\\
& c_3 = g_2 + p_2.(g_1 + p_1.(g_0 + p_0.c_0))
\\
& \dots
\end{aligned}
$$

---

#### Hierarchical Adder

> [!NOTE]
> A hierarchical `carry-lookahead` with `ripple-carry` between blocks.

![Hierarchical Adder](assets/hierarchical_adder.png)

Detail:

![Hierarchical Adder Detail](assets/hierarchical_adder_detail.png)

---

#### Multiplier

![Multiplier](assets/multiplier.png)

Detail:

![Multiplier Detail](assets/multiplier_detail.png)

##### Positive Multiplication Example

![Multiplier Example Positive](assets/multiplier_example_positive.png)

##### Negative Multiplication Example

![Multiplier Example Negative](assets/multiplier_example_negative.png)

---

### Signed

![Signed Number](assets/signed_number.png)

Types:

![Signed Number Types](assets/signed_number_types.png)

#### Sign and Magnitude

##### Adder/Subtractor

> [!NOTE]
> Add/Sub => `Change Sign`
>
> Use **Adder** with **XOR** controller

$$
\begin{aligned}
& A = a_{n-1} a_{n-2} \dots a_{1} a_{0}
\\
& B = b_{n-1} b_{n-2} \dots b_{1} b_{0}
\end{aligned}
$$

```js
let A_Sign = A[n - 1];
let B_Sign = B[n - 1];

if (A_Sign == B_Sign) {
    // Add => A,B are + or -
    Control = "ADD";
} else {
    // Sub => A is +, B is - or reverse
    Control = "SUB";
}
```

![Sign And Magnitude ALU](assets/sign_and_magnitude_alu.png)

---

#### 1's Complement

##### Adder/Subtractor

> [!NOTE]
> Add/Sub => `Change Sign`
>
> Use **Adder** with **Last Carry** add

![1'S Complement ALU](assets/ones_complement_alu.png)

---

#### 2's Complement

##### Adder/Subtractor

> [!NOTE]
> Add/Sub => `Change Sign`
>
> Use **Adder** with **Last Carry** ignore

![2'S Complement ALU](assets/twos_complement_alu.png)

---

## Fixed point

$$
\begin{aligned}
& B = b_{n-1} \: b_{n-2} \: \dots \: b_{1} \: b_{0} . b_{-1} b_{-2} \dots b_{-k}
\\
\\
& Values(B) = \sum_{i=-k}^{n-1} b_{i} * 2^{i}
\end{aligned}
$$

### Adder/Subtractor

> [!NOTE]
> Add/Sub => `Change Sign`
>
> Use **Adder** of type `Sign and Magnitude`

---

## Floating point

$$
\begin{aligned}
& Value = Mantissa * R^{Exponent}
\end{aligned}
$$

### Single Precision (Excess-127)

![Floating Point Single](assets/floating_point_single.png)

$$
\begin{aligned}
& Exponent = E - 127
\\
\\
& Value = \plusmn 1.M * 2^{E - 127}
\end{aligned}
$$

---

### Double Precision (Excess-1023)

![Floating Point Double](assets/floating_point_double.png)

$$
\begin{aligned}
& Exponent = E - 1023
\\
\\
& Value = \plusmn 1.M * 2^{E - 1023}
\end{aligned}
$$

---

## Binary Coded Decimal (BCD)

![BCD](assets/bcd.png)

### Adder/Subtractor

```js
let sum = X + Y + C;
if (sum > 9) {
    carry = 1;
    return sum + 6;
} else {
    carry = 0;
    return sum;
}
```

![BCD Adder](assets/bcd_adder.png)

Detail:

![BCD Adder Detail](assets/bcd_adder_detail.png)

Example:

![BCD Adder Example](assets/bcd_adder_example.png)

---
