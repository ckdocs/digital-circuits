# Sequential Circuits

![Sequential Circuit](assets/sequential_circuit.jpg)

Every memory elements contains two `NOT` gates:

![Memory Element](assets/memory_element.png)

> [!TIP]
>
> Combinational Circuit Output => **Present Input**
>
> Sequential Circuit Output => **Present Input** + **Present State**

| Combinational Circuits                   | Sequential Circuits                                          |
| ---------------------------------------- | ------------------------------------------------------------ |
| Outputs depend only on `present inputs`. | Outputs depend on both `present inputs` and `present state.` |
| Feedback path is not present.            | Feedback path is present.                                    |
| Memory elements are not required.        | `Memory elements` are required.                              |
| Clock signal is not required.            | `Clock signal` is required.                                  |
| `Easy` to design.                        | `Difficult` to design.                                       |

## Sequential Circuits Types

### Asynchronous sequential circuits

Memory elements may use another wire instead of `Clock Signal` wire (**does not sync with clock signal**)

Example:

![Asynchronous Binary Up Counter](assets/asynchronous_binary_up_counter.jpg)

### Synchronous sequential circuits

Memory elements only use `Clock Signal` wire (**sync with clock signal**)

Example:

![Synchronous Binary Up Counter](assets/synchronous_binary_up_counter.jpg)

### Synchronous vs Asynchronous

![Synchronous vs Asynchronous](assets/synchronous_vs_asynchronous.png)

1. Asynchronous `time gaps` getting `larger`
2. Synchronous `time gaps` are `fixed`

---

## Clock Signal

A periodic signal that syncs the `Memory Elements` for `Read` and `Write` time, that has a `Frequency`.

![Clock Signal 1](assets/clock_signal_1.jpg)

![Clock Signal 2](assets/clock_signal_2.jpg)

---

## Triggering Types

> [!NOTE]
>
> **Triggering**: Means the time that `Memory Element` started changing it's `state`

### Level Triggering

Some memory elements will change their state in `Positive Level` or `Negative Level`

1. SR Latch
2. D Latch

#### Positive Level Triggering

![Positive Level Triggering](assets/positive_level_triggering.jpg)

#### Negative Level Triggering

![Negative Level Triggering](assets/negative_level_triggering.jpg)

---

### Edge Triggering

1. Master-Slave D Flip-Flop
2. SR Flip-Flop
3. T Flip-Flop
4. JK Flip-Flop

#### Positive Edge Triggering

![Positive Edge Triggering](assets/positive_edge_triggering.jpg)

#### Negative Edge Triggering

![Negative Edge Triggering](assets/negative_edge_triggering.jpg)

---

### Level Sensitive vs Edge Triggering

![Level Sensitive vs Edge Triggering](assets/level_sensitive_vs_edge_triggered.png)

| Level Sensitive                                  | Positive Edge Triggering                       | Negative Edge Triggering                        |
| ------------------------------------------------ | ---------------------------------------------- | ----------------------------------------------- |
| `Change` state `multiple times` when is in level | Only change `before` the `positive edge` comes | Only changes `before` the `negative edge` comes |

---

## Latch

> [!TIP]
>
> Latches are `level sensitive`

**Basic Latch** contains:

1. Memory Element (Two NOT gates)
2. Set Wire
3. Reset Wire

![Basic Latch](assets/basic_latch.png)

### SR Latch

A `Basic Latch` with `Clock Signal` wire called `SR Latch (Set-Reset Latch)`

![SR Latch](assets/sr_latch.png)

#### Symbol

1. `Clk` wire
2. `S`, `R` wires

![SR Latch Symbol](assets/sr_latch_symbol.png)

---

### D Latch

A `SR Latch` with connected `Set, Reset` wires using `NOT` gate called `D Latch(Data Latch)`

![D Latch](assets/d_latch.png)

#### Symbol

1. `Clk` wire
2. `D` wire

![D Latch Symbol](assets/d_latch_symbol.png)

---

## Flip-Flop

> [!TIP]
>
> Flip-Flops are `edge triggering`
>
> Flip-Flops contains `two latches`

![Flip-Flops](assets/flip_flops.png)

### D Flip-Flop (Data)

#### Negative Edge D Flip-Flop (Master-Slave)

![Negative Edge D Flip-Flop](assets/negative_edge_d_flip_flop.png)

##### Symbol

1. `Triangle-Not` wire
2. `D` wire

![Negative Edge D Flip-Flop Symbol](assets/negative_edge_d_flip_flop_symbol.png)

##### Clear-Preset

Force to `clear` or `set` wires:

![Negative Edge D Flip-Flop Clear-Preset](assets/negative_edge_d_flip_flop_clear_preset.png)

---

#### Positive Edge D Flip-Flop

![Positive Edge D Flip-Flop](assets/positive_edge_d_flip_flop.png)

##### Symbol

1. `Triangle` wire
2. `D` wire

![Positive Edge D Flip-Flop Symbol](assets/positive_edge_d_flip_flop_symbol.png)

##### Clear-Preset

Force to `clear` or `set` wires:

![Positive Edge D Flip-Flop Clear-Preset](assets/positive_edge_d_flip_flop_clear_preset.png)

---

### T Flip-Flop (Toggle)

A `D Flip-Flop` with a combinational circuit:

$$
\begin{aligned}
& D = (\overline{Q(t)} \quad . \quad T) \quad + \quad (Q(t) \quad . \quad \overline{T})
\\
& D = Q(t) \quad \oplus \quad T
\end{aligned}
$$

![T Flip-Flop](assets/t_flip_flop.png)

#### Symbol

1. `Triangle` wire
2. `T` wire

![T Flip-Flop Symbol](assets/t_flip_flop_symbol.png)

---

### JK Flip-Flop

A `D Flip-Flop` with a combinational circuit:

$$
\begin{aligned}
& D = (\overline{Q(t)} \quad . \quad J) \quad + \quad (Q(t) \quad . \quad \overline{K})
\end{aligned}
$$

![JK Flip-Flop](assets/jk_flip_flop.png)

#### Symbol

1. `Triangle` wire
2. `J`, `K` wire

![JK Flip-Flop Symbol](assets/jk_flip_flop_symbol.png)

---

### Flip-Flops Conversion

![Flip-Flops Table](assets/flip_flops_table.png)

#### SR to D

![Flip-Flops SR to D](assets/flip_flops_sr_to_d.jpg)

#### D to T

![Flip-Flops D to T](assets/flip_flops_d_to_t.jpg)

#### T to D

![Flip-Flops T to D](assets/flip_flops_t_to_d.jpg)

#### JK to T

![Flip-Flops JK to T](assets/flip_flops_jk_to_t.jpg)

---

## Register

A sequence of `D Flip-Flops`

### Shift Register

> [!NOTE]
> Shift registers are used to convert `Serial` data to `Parallel` data or reverse or change `Frequency` of `Serial` data

1. Left Shift Register
2. Right Shift Register

![Shift Register](assets/shift_register.png)

#### Serial In - Serial Out (SISO)

![Shift Register SISO](assets/shift_register_siso.png)

#### Serial In - Parallel Out (SIPO)

![Shift Register SIPO](assets/shift_register_sipo.jpg)

#### Parallel In - Serial Out (PISO)

![Shift Register PISO](assets/shift_register_piso.jpg)

#### Parallel In - Parallel Out (PIPO)

![Shift Register PIPO](assets/shift_register_pipo.png)

---

### Ring Counter

`Ring Counter` is a `Shift Register` that connected like a `Ring` and will repeat a pattern in a circle.

> [!TIP]
>
> Ring counter is a `Shift Register` that `Q(t)` wire of `Last Flip-Flop` connected to `D` wire of `First Flip-Flop`

![Ring Counter](assets/ring_counter.jpg)
![Ring Counter](assets/ring_counter.png)

---

### Johnson Counter (Twisted Ring Counter)

> [!TIP]
>
> Johnson counter is a `Shift Register` that `Q(t)'` wire of `Last Flip-Flop` connected to `D` wire of `First Flip-Flop`

![Johnson Counter](assets/johnson_counter.png)

---

## Counter

A sequence of `T Flip-Flops`

1. Up Counter
2. Down Counter

### Asynchronous Counter

#### Asynchronous Binary Up Counter

Up Counter => **Q(t) => Clock Q(t+1)**

![Asynchronous Binary Up Counter](assets/asynchronous_binary_up_counter.jpg)

#### Asynchronous Binary Down Counter

Down Counter => **Q(t)' => Clock Q(t+1)**

![Asynchronous Binary Down Counter](assets/asynchronous_binary_down_counter.jpg)

### Synchronous Counter

#### Synchronous Binary Up Counter

Up Counter => **(Q(t) and Q(t-1) and ...) => T(t+1)**

![Synchronous Binary Up Counter](assets/synchronous_binary_up_counter.jpg)

#### Synchronous Binary Down Counter

Down Counter => **(Q(t)' and Q(t-1)' and ...) => T(t+1)**

![Synchronous Binary Down Counter](assets/synchronous_binary_down_counter.jpg)

---
